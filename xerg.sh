#bin/bash


# Xerg - simple xresources theme manager
# Author Felipe Freire
# 

# 20220726 1st version v0.1
# 20230307 2nd version v0.2

xres=~/.Xresources


# variable xres exists or message
[ "$xres" ] || {
	echo "xerg: missing Xresource file."
}


get_color() {

	# get line from $xres, cut second field. delimiter: "#"
	grep "color$1:.*#" "$xres" | cut -d"#" -f2
}


change_color() {

	# checking for exactly 2 arguments
	if [ "$#" -eq "2" ]; then
		# get color that matches ...$1 
		color=$(get_color $1)

		# variable color exists or return
		[ "$color" ] || return

		# sed -i edits the xres file
		# update
		sed -i "s/$color/$2/" "$xres"	
		xrdb $xres

	# if not two arguments, displays error message
	else
		echo "i need exactly two arguments: [ hex color ] [ new hex color ]"
	fi
}


xchange() {

	# cecking for exactly 1 argument
	if [ "$#" -eq "1" ]; then

		# initializing counter. it will localize
		# a color by number inside loop
		local i=0

		# loop
		# it uses change_color function
		while read line; do
			change_color $i $line
			
			# $i incrrement
			# echo changes 
			i=$((i+1))

		# feed the read loop
		# end loop
		done < $1 
	fi
}
 
xfer() {
	for n in {0..15}; do
		echo "$(get_color $n)"
	done
}
