Xerg is a function lib to automate changes in Xresources

Author:	Felipe Freire
repo:	https://gitlab.com/felbs/xerg.git

1. move themes folder to .Xresouces location, changing themes to .themes (hidden)
$ mv themes ~/.themes

.2 source xerg.sh
$ source xerg.sh

3 xchange .Xresources colors
$ xchange ~/.themes/dracula.txt

-- Functions --


get_colors -> prints all hexadecimal colors from Xresource files. Not back/fore grounds


change_color -> it changes a single color. You must not have duplicates on Xresources
                or the program will fail.. a very boring fail


xchange -> it switches all colors to the ones provided by a file. As it uses 
           change_color function, you must not have duplicates


-- Themes --

themes -> colors are ordered by crescent order: head 1, bottom 15

-- Usage --

```bash
$ get_color 3

 #000000


$ change_color 3 222222


$ xchange themes/dracula.txt
```
